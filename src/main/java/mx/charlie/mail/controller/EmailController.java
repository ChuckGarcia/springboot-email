package mx.charlie.mail.controller;

import mx.charlie.mail.dto.MailRequestDTO;
import mx.charlie.mail.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;

@RestController
@RequestMapping("/sendmail")
public final class EmailController {

    private final EmailService emailService;

    @Autowired
    public EmailController(final EmailService emailService) {
        this.emailService = emailService;
    }

    @PostMapping("/")
    public ResponseEntity<String> sendMail(@RequestBody MailRequestDTO mailRequestDTO) {
        try {
            emailService.sendEmail(mailRequestDTO.getTo(), mailRequestDTO.getName());
        } catch (MessagingException e) {
            e.printStackTrace();
            return new ResponseEntity<>("something failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>("mail sent", HttpStatus.OK);
    }
}

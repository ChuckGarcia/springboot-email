package mx.charlie.mail.service.impl;

import mx.charlie.mail.service.EmailService;
import mx.charlie.mail.template.HelloMailTemplate;
import mx.charlie.mail.template.Mail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender javaMailSender;

    @Autowired
    public EmailServiceImpl(final JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void sendEmail(String to, String receiverName) throws MessagingException {
        HelloMailTemplate helloMailTemplate = new HelloMailTemplate(to, receiverName);
        sendEmail(helloMailTemplate);
    }

    @Override
    public void sendEmail(Mail emailBody) throws MessagingException {
        if(emailBody == null) {
            throw new IllegalArgumentException("email body must not be null.");
        }
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
        String[] mails = new String[emailBody.getToList().size()];
        mails = emailBody.getToList().toArray(mails);
        helper.setTo(mails);
        helper.setText(emailBody.getMessage(), true);
        helper.setSubject(emailBody.getSubject());
        javaMailSender.send(mimeMessage);
    }
}

package mx.charlie.mail.service;

import mx.charlie.mail.template.Mail;

import javax.mail.MessagingException;

public interface EmailService {
    void sendEmail(String to, String receiverName) throws MessagingException;

    void sendEmail(Mail emailBody) throws MessagingException;
}

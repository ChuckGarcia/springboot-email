package mx.charlie.mail.template;

import java.util.List;

public class Mail {

    private List<String> toList;
    private List<String> ccList;
    private List<String> bccList;
    private String subject;
    private String message;
    private List<Attachment> attachmentList;


    /**
     * Get bccList value.
     *
     * @return bccList
     */
    public List<String> getBccList() {
        return bccList;
    }

    /**
     * Set new bccList value.
     */
    public void setBccList(List<String> bccList) {
        this.bccList = bccList;
    }

    /**
     * Get toList value.
     *
     * @return toList
     */
    public List<String> getToList() {
        return toList;
    }

    /**
     * Set new toList value.
     */
    public void setToList(List<String> toList) {
        this.toList = toList;
    }

    /**
     * Get ccList value.
     *
     * @return ccList
     */
    public List<String> getCcList() {
        return ccList;
    }

    /**
     * Set new ccList value.
     */
    public void setCcList(List<String> ccList) {
        this.ccList = ccList;
    }

    /**
     * Get attachmentList value.
     *
     * @return attachmentList
     */
    public List<Attachment> getAttachmentList() {
        return attachmentList;
    }

    /**
     * Set new attachmentList value.
     */
    public void setAttachmentList(List<Attachment> attachmentList) {
        this.attachmentList = attachmentList;
    }

    public Mail() {

    }

    /**
     * Get subject value.
     *
     * @return subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Set new subject value.
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * Get message value.
     *
     * @return message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Set new message value.
     */
    public void setMessage(String message) {
        this.message = message;
    }

}

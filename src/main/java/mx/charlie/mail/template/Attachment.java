package mx.charlie.mail.template;

public class Attachment {

    private String attachmentFileName;
    private byte[] attachment;

    /**
     * Get attachmentFileName value.
     *
     * @return attachmentFileName
     */
    public String getAttachmentFileName() {
        return attachmentFileName;
    }

    /**
     * Set new attachmentFileName value.
     */
    public void setAttachmentFileName(String attachmentFileName) {
        this.attachmentFileName = attachmentFileName;
    }

    /**
     * Get attachment value.
     *
     * @return attachment
     */
    public byte[] getAttachment() {
        return attachment;
    }

    /**
     * Set new attachment value.
     */
    public void setAttachment(byte[] attachment) {
        this.attachment = attachment;
    }
}

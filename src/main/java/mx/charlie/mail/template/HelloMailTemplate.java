package mx.charlie.mail.template;

import j2html.tags.ContainerTag;
import lombok.extern.log4j.Log4j2;

import java.nio.charset.StandardCharsets;
import java.util.Collections;

import static j2html.TagCreator.body;
import static j2html.TagCreator.div;
import static j2html.TagCreator.document;
import static j2html.TagCreator.h1;
import static j2html.TagCreator.head;
import static j2html.TagCreator.html;
import static j2html.TagCreator.i;
import static j2html.TagCreator.link;
import static j2html.TagCreator.meta;
import static j2html.TagCreator.p;
import static j2html.TagCreator.span;
import static j2html.TagCreator.style;

/**
 * HelloMailTemplate
 *
 * @author cagarcia
 */
@Log4j2
public final class HelloMailTemplate extends Mail {

    /**
     * Receiver name
     */
    private final String receiverName;

    /**
     * Initialize the default html template.
     *
     * @param receiverName name of who receives the message
     * @param receiverMail mail to send message
     */
    public HelloMailTemplate(final String receiverMail, final String receiverName) {
        this.receiverName = receiverName;
        this.setToList(Collections.singletonList(receiverMail));
        this.setMessage(document(build()));
        this.setSubject("Hello " + receiverName);
    }

    /**
     * Builds the template.
     *
     * @return ContainerTag with default html template
     */
    private ContainerTag build() {
        return html(
                head(
                        meta().withCharset(StandardCharsets.UTF_8.name()),
                        link().withRel("stylesheet")
                                .withHref("https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"),
                        link().withRel("stylesheet")
                                .withHref("https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css")
                ),
                body(
                        div(
                                style(".chuckcss{" +
                                        "padding-top:10vw;" +
                                        "padding-right:10vw;" +
                                        "padding-bottom:10vw;" +
                                        "padding-left:10vw;" +
                                        "background-color: rgb(255, 123, 41);}" +
                                        ".chuckcss-child{" +
                                        "background-color:rgb(255,255,255);" +
                                        "padding-top:5vw;" +
                                        "padding-right:10vw;" +
                                        "padding-bottom:5vw;" +
                                        "padding-left:10vw;" +
                                        "text-align: center;}"),
                                div(
                                        div(
                                                div(
                                                        h1(
                                                                span("Hello " + receiverName + "!")
                                                        )
                                                ).withClass("row"),
                                                div(
                                                        p(
                                                                span().withText("It works! Thanks for use my demo")
                                                        ),
                                                        i().withClass("fa fa-check")
                                                                .withStyle("font-size: 133px; color: rgb(54, 191, 125);")
                                                ).withClass("row")
                                        ).withClass("chuckcss-child")
                                ).withClass("chuckcss")
                        ).withClass("container")
                )
        );

    }
}

package mx.charlie.mail.service;

import lombok.extern.log4j.Log4j2;
import mx.charlie.mail.service.impl.EmailServiceImpl;
import mx.charlie.mail.template.Mail;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@Log4j2
public class EmailServiceTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private JavaMailSender javaMailSender;

    private EmailService emailService;

    @Before
    public void init() {
        emailService = new EmailServiceImpl(javaMailSender);
    }

    @Test
    public void sendEmail() throws MessagingException {
        Mail mail = new Mail();
        List<String> to = new ArrayList<>();
        to.add("risko.wan@gmail.com");
        mail.setToList(to);
        mail.setSubject("Hello world");
        mail.setMessage("<br/><b>It works!!</b><br/>");
        MimeMessage mimeMessage = Mockito.mock(MimeMessage.class);
        when(javaMailSender.createMimeMessage()).thenReturn(mimeMessage);
        emailService.sendEmail(mail);
        verify(javaMailSender, times(1)).send(mimeMessage);
    }

    @Test
    public void sendEmailTo() throws MessagingException {
        MimeMessage mimeMessage = Mockito.mock(MimeMessage.class);
        when(javaMailSender.createMimeMessage()).thenReturn(mimeMessage);
        emailService.sendEmail("example@domain.com", "Carlos");
        verify(javaMailSender, times(1)).send(mimeMessage);
    }

    @Test
    public void sendEmailWithNullArg() throws MessagingException {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("email body must not be null.");
        emailService.sendEmail(null);
    }

}

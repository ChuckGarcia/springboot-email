package mx.charlie.mail.controller;

import lombok.extern.log4j.Log4j2;
import mx.charlie.mail.service.EmailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.mail.MessagingException;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @autor risko
 */
@Log4j2
@RunWith(SpringRunner.class)
@WebMvcTest(EmailController.class)
public class EmailControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmailService emailService;

    @Test
    public void testController() throws Exception {
        String messageExpected = "mail sent";
        mockMvc.perform(post("/sendmail/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"to\": \"risko.wan@live.com\",\n" +
                                "\"name\": \"Chuck\"}"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(messageExpected));
        verify(emailService, times(1)).sendEmail("risko.wan@live.com", "Chuck");
    }

    @Test
    public void testControllerErrorCase() throws Exception {
        String messageExpected = "something failed";
        doThrow(MessagingException.class).when(emailService).sendEmail(anyString(), anyString());
        mockMvc.perform(post("/sendmail/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"to\": \"\",\n" +
                                "\"name\": \"\"}"))
                .andDo(print())
                .andExpect(status().isInternalServerError())
                .andExpect(content().string(messageExpected));
        verify(emailService, times(1)).sendEmail("", "");
    }


}
